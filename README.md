# Chrome Apps

These mini extensions load web apps as applicatoins into chrome.
This can be used in Windows 10 and Ubuntu to give each website their own icon
making it much easier to start and alt-tab through them

# Usage

1. Open Chrome
2. Go to chrome://extensions
3. Enable Developer mode
4. Use "Load Unpacked" to add one of the subdirectories
5. Go to chrome://apps
6. Right click and enable, "open as window"
7. Right click and "Create shortcuts..."

Webapp should now be available in the start menu and load like a regular
application

# Create new sets with generate.sh

Download icon and place next to generate.sh as icon.png then run the script

./generate.sh <name> <url>

This will create a basic setup. 

./generate.sh Paper https://paper.dropbox.com/
