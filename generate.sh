#!/bin/bash
name=$1
id=`echo $name | tr '[:upper:]' '[:lower:]'`
url=$2
domain=`echo $url | awk -F[/:] '{print $4}'`


echo "Name: $name"
echo "ID: $id"

if [ ! -d $id ]; then
  mkdir $id
  cat > $id/manifest.json <<EOF
{
 "name": "${name}",
 "description": "${name}",
 "version": "1.0.0",
 "manifest_version": 2,
 "icons": {
 "128": "${id}_128.png",
 "512": "${id}_512.png"
 },
 "app": {
  "urls": [
   "http://${domain}",
   "https://${domain}"
  ],
  "launch": {
   "web_url": "${url}"
  }
 }
}
EOF
  if [ -f icon.png ]; then
	  convert -resize 128x128 icon.png ${id}/${id}_128.png
	  convert -resize 512x512 icon.png ${id}/${id}_512.png
	  mv icon.png ${id}/${id}.png
  fi
else
  echo "Directory $id already exists, skipping"
fi
